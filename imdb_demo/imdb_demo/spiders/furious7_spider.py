import scrapy
import urlparse

from imdb_demo.items import MovieItem
from imdb_demo.items import ActorItem

class Furious7Spider(scrapy.Spider):
    name = "furious7"
    allowed_domains = ["imdb.com"]
    start_urls = [
        "http://www.imdb.com/title/tt2820852/"
    ]

    def parse(self, response):

        item = MovieItem()

        # Chrome's element inspector generated the XPath expressions.
        item['title'] = response.xpath('//*[@id="overview-top"]/h1/span[1]/text()').extract()
        item['director'] = response.xpath('//*[@id="overview-top"]/div[4]/a/span/text()').extract()
        item['rating'] = response.xpath('//*[@id="overview-top"]/div[3]/div[1]/text()').extract()
        item['synopsis'] = response.xpath('//*[@id="overview-top"]/p[2]/text()').extract()
        item['image_urls'] = response.xpath('//*[@id="img_primary"]/div[1]/a/img/@src').extract()

        yield item

        cast_links = response.xpath('//*[@id="titleCast"]/table/tr/td[2]/a/@href').extract()

        for relative_link in cast_links:

            absolute_link = urlparse.urljoin(response.url, relative_link)
            yield scrapy.http.Request(absolute_link, callback=self.parse_actor)


    def parse_actor(self, response):

        item = ActorItem()

        item['name'] = response.xpath('//*[@id="overview-top"]/h1/span[1]/text()').extract()
        item['birth_date'] = response.xpath('//*[@id="name-born-info"]/time/@datetime').extract()

        yield item
