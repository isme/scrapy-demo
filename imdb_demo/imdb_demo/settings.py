# -*- coding: utf-8 -*-

# Scrapy settings for imdb_demo project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'imdb_demo'

SPIDER_MODULES = ['imdb_demo.spiders']
NEWSPIDER_MODULE = 'imdb_demo.spiders'

ITEM_PIPELINES = {'scrapy.contrib.pipeline.images.ImagesPipeline': 1}
IMAGES_STORE = '.'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'imdb_demo (+http://www.yourdomain.com)'
