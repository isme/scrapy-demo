# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class MovieItem(scrapy.Item):
    title = scrapy.Field()
    director = scrapy.Field()
    rating = scrapy.Field()
    synopsis = scrapy.Field()
    images = scrapy.Field()
    image_urls = scrapy.Field()


class ActorItem(scrapy.Item):
    name = scrapy.Field()
    birth_date = scrapy.Field()
