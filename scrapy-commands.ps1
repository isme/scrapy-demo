﻿# Create new Scrapy project.
scrapy startproject imdb_demo

# Go to the root of the Scrapy project.
Set-Location .\imdb_demo

# Run the furious7 spider.
scrapy crawl furious7

# Try selectors in the shell.

scrapy shell "http://www.imdb.com/title/tt2820852/"
 
# Store the scraped data in a folder called "out".
# Scrapy creates the folder if it does not exist.
scrapy crawl furious7 `
  --output-format=json `
  --output=out\furious7.json `
  --set=IMAGES_STORE=out

# Remove the output folder if it exists.
# This allows for a clean run during a demo.
Remove-Item -Path out -Force