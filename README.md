# Scrapy Demo

This repo contains a Scrapy project I prepared for a discussion on Web Scraping at the Skyscanner Python Guild meeting on 2015-04-16.

The demo scraper is based on the [Scrapy tutorial](http://doc.scrapy.org/en/latest/intro/tutorial.html). At the time of writing, the Scrapy version is 0.24.5.

## Spec

Chris McCluskey and I agreed the following spec for our web scraping projects.

    Go to the IMDB page for Fast and the Furious 7.
    Extract the top 3 actors and their birth dates.
    Extract the director, rating, synopsis, and movie poster.
    Stretch goal: output to JSON

## Run the demo

See [scrapy-commands.ps1](https://bitbucket.org/isme/scrapy-demo/src/master/scrapy-commands.ps1?at=master) for commands to run the demo.

## Understanding the code

View the commit history to see how the project was developed.

To help understand the thought process behind the changes, see the Developer Notes on the wiki.

Read the Scrapy manual. It's really quite good!

## Getting started with Scrapy on Windows 7

* Install [Python 2.7](https://www.python.org/downloads/).
* Install [Scrapy](https://pypi.python.org/pypi/Scrapy).
* Install [Python for Windows Extensions](http://sourceforge.net/projects/pywin32/)
* Install [Pillow](https://pypi.python.org/pypi/Pillow/2.8.1)
* Install [service_identity](https://pypi.python.org/pypi/service_identity).

The [Scrapy installation instructions](http://doc.scrapy.org/en/latest/intro/install.html) has all the information you need to start using Scrapy on Windows 7.

## Notes

Scrapy works with Python 2.7. There "are plans to support Python 3.3+".

The service_identity package is optional, but it stops Scrapy from generating an annoying error [like this](http://stackoverflow.com/questions/24089484/python-no-module-named-service-identity):

> :0: UserWarning:
> You do not have a working installation of the service_identity module: 'No module named service_identity'.
> Please install it from <https://pypi.python.org/pypi/service_identity> and make sure all of its dependencies are satisfied.
> Without the service_identity module and a recent enough pyOpenSSL to support it, Twisted can perform only rudimentary TLS client hostname verification.
> Many valid certificate/hostname mappings may be rejected.

For some reason, pip's dependency resolution does not install it by default.

Project names must begin with a letter and contain only letter, numbers, and underscores.

Scrapy shell advertises compatibility with IPython, but it doesn't work for me.

The response object is always set to None in IPython, which makes debugging impossible!